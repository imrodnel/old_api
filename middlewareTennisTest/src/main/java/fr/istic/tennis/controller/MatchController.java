package fr.istic.tennis.controller;

import fr.istic.tennis.model.sp.DebutMatchProcedure;
import fr.istic.tennis.model.sp.FinMatchProcedure;
import fr.istic.tennis.model.sp.InfoMatchProcedure;
import fr.istic.tennis.model.sp.InfosTableauProcedure;
import fr.istic.tennis.model.sp.InfosSuperMatchsProcedure;
import fr.istic.tennis.model.sp.InitMatchProcedure;
import fr.istic.tennis.model.sp.ListeMatchsAVenirProcedure;
import fr.istic.tennis.model.vo.EndMatch;
import fr.istic.tennis.model.vo.Response;
import fr.istic.tennis.model.vo.StartMatch;
import fr.istic.tennis.model.vo.Tableau;
import java.util.ArrayList;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Romain
 */

@Controller
@RequestMapping(value = "/match")
public class MatchController {
    
    @Autowired
    FinMatchProcedure finMatchProcedure;

    @Autowired
    InitMatchProcedure initMatchProcedure;
    
    @Autowired
    DebutMatchProcedure debutMatchProcedure;
    
    @Autowired
    ListeMatchsAVenirProcedure listeMatchsAvenirProcedure;

    @Autowired
    InfosTableauProcedure infosTableauProcedure;

    @Autowired
    InfosSuperMatchsProcedure infosSuperMatchsProcedure;
    
    @Autowired
    InfoMatchProcedure infoMatchProcedure;
    
    @RequestMapping(method = RequestMethod.POST, value = "/finMatch")
    public @ResponseBody Response endMatch(@RequestBody EndMatch finMatch){        
        try{    
            //System.out.println("----------- updateTotScore ---------------");

            finMatchProcedure.execute_update(finMatch); 
        }
        catch(Exception e){            
            return new Response(-1, "Erreur dans la fin du match : " + e.getMessage());
        }        
        return new Response(0, "Fin du match ,OK");
    }
    
    @RequestMapping(method = RequestMethod.POST, value = "/debut")
    public @ResponseBody Response debutMatch(@RequestBody StartMatch startMatch){        
        try{   

            //System.out.println("----------- updateTotScore ---------------");

            //initMatchProcedure.execute_update(startMatch);
            debutMatchProcedure.execute_update(startMatch);
        }
        catch(Exception e){            
            return new Response(-1, "Erreur dans le start du match : " + e.getMessage());
        }        
        return new Response(0, "Start du match ,OK");
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/info/{ident}")
    public @ResponseBody HashMap<Object, Object> infoMatch(@PathVariable int ident){ 
                //System.out.println("----------- updateTotScore ---------------");

        return (HashMap<Object, Object>)infoMatchProcedure.execute_query(ident);
    }
    
     @RequestMapping(method = RequestMethod.POST, value = "/prochainsMatchs")
    public @ResponseBody  ArrayList<HashMap<Object, Object>> listeProchainMatch(@RequestBody Tableau tableau){        
               // System.out.println("----------- updateTotScore ---------------");

         return ( ArrayList<HashMap<Object, Object>>)listeMatchsAvenirProcedure.execute_query(tableau);
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/infosTableau")
    public @ResponseBody Object infosTableau(){
       // System.out.println("----------- infosTableau ---------------");
        return infosTableauProcedure.execute_query(null);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/super/{etape}")
    public @ResponseBody Object superMatch(@PathVariable int etape){
       // System.out.println("----------- superMatch ---------------");
        try{       
            String param = "";
            switch(etape){
                case 4:
                    param="1/4";
                    break;
                case 2:
                    param="1/2";
                    break;
                case 1:
                    param="F";
                    break;
                default:
                    param="F";
            }            
            return infosSuperMatchsProcedure.execute_query(param);
        }
        catch(Exception e){ 
           // System.out.println("Exception "+e.toString());            
           // System.out.println("Exception2 "+e.getLocalizedMessage());                       
            return new Response(-1, "Erreur dans superMatch : " + e.getMessage());
        }        
    }
}
