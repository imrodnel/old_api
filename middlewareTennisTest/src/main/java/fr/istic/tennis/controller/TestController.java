package fr.istic.tennis.controller;

import fr.istic.tennis.model.vo.Request;
import fr.istic.tennis.model.vo.Response;
import fr.istic.tennis.model.TestBaseModel;
import fr.istic.tennis.model.TestConnection;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Monik
 */

@Controller
@RequestMapping(value = "/test")
public class TestController {
    
    @Autowired    
    TestBaseModel testBaseModel;
    
    @Autowired
    TestConnection testConnection;
    
    @RequestMapping(method = RequestMethod.GET, value = "/ping")
    public @ResponseBody Response ping() {
        
        return new Response();
    }
    
    @RequestMapping(method = RequestMethod.POST, value = "/nombres/ajouterNombre")
    public @ResponseBody Response addNumber(@RequestBody Request req) {
        
        int insert = testBaseModel.insert(req.getNombre());
        
        Response retour = new Response();
        retour.setCode(req.getNombre());
        retour.setMessage("Nombre ajouté");
 
        return retour;
    }
    
    @RequestMapping(method = RequestMethod.GET, value = "/nombres/last")
    public @ResponseBody Response lastNumber() {
        
        int code = testBaseModel.query();
        
        Response resp = new Response();
        resp.setCode(code);
        resp.setMessage("Dernier nombre inséré dans la BDD");
        
        return resp;
    }
    
    @RequestMapping(value = "/ping_cnx")
    public @ResponseBody Response ping_cnx() {
        
        testConnection.testConnection();
        return new Response();
    }
}
