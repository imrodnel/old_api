package fr.istic.tennis.controller;

import fr.istic.tennis.model.sp.DeletePointProcedure;
import fr.istic.tennis.model.sp.IdMatchOldPointProcedure;
import fr.istic.tennis.model.sp.UpdateTotProcedure;
import fr.istic.tennis.model.sp.UpdateSetProcedure;
import fr.istic.tennis.model.vo.Response;
import fr.istic.tennis.model.vo.ResponsePoint;
import fr.istic.tennis.model.vo.Score;
import fr.istic.tennis.model.vo.UpdateSet;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Monik
 */
@Controller
@RequestMapping(value = "/score")
public class ScoreController {

    @Autowired
    UpdateTotProcedure updateTotProcedure;
    @Autowired
    UpdateSetProcedure updateSetProcedure;
    @Autowired
    IdMatchOldPointProcedure idMatchOldPointProcedure;
    
    @Autowired
    DeletePointProcedure deletepointProcedure;

    @RequestMapping(method = RequestMethod.POST, value = "/update")
    public @ResponseBody ResponsePoint updateTotScore(@RequestBody Score scoreData) {
        //System.out.println("----------- updateTotScore ---------------");
        //System.out.println(scoreData);
        ResponsePoint r = new ResponsePoint();
        try {
            int idOldPoint = idMatchOldPointProcedure.execute_query(scoreData.getIdMatch());
            if (idOldPoint != -100) { //il n'ya pas d'exception
                if (scoreData.getIdPoint() == idOldPoint) {//le point est déjà insère
                    r = new ResponsePoint(idOldPoint, -1, "Le point est déjà insère");
                    return r;
                }
                if (scoreData.getIdPoint() < idOldPoint) {
                    //System.out.println("idOldPoint < idPoint ");
                    r.setIdOldPoint(idOldPoint);
                }

                if (scoreData.getIdPoint() > (idOldPoint + 1)) {
                    //System.out.println("idPoint  correct");
                    r.setIdOldPoint(idOldPoint);
                }
            } else { //il y a une exception dans IdMatchtoIdPoint
                r = new ResponsePoint(-1, -1, "Erreur dans IdMatchtoIdPoint ");
                return r;
            }
            updateTotProcedure.execute_update(scoreData);
        } catch (Exception e) {
            r = new ResponsePoint(-1, -1, "Erreur dans updateTotScore : " + e.getMessage());
            return r;
        }
        r.setMessage("Update Score OK");
        return r;
    }

    @RequestMapping(method = RequestMethod.POST, value = "/majSet")
    public @ResponseBody Response majSet(@RequestBody UpdateSet donneesMaj) {

        try {

            updateSetProcedure.execute_update(donneesMaj);
        } catch (Exception e) {

            return new Response(-1, "Erreur dans majSet : " + e.getMessage());
        }

        return new Response(0, "Score des sets mis à jour");
    }
    
    @RequestMapping(method = RequestMethod.POST, value = "/supprPoint")
    public @ResponseBody Response deletePoint(@RequestBody Score score) {

        try {

            deletepointProcedure.execute_update(score);
        } catch (Exception e) {

            return new Response(-1, "Erreur dans la suppression de point : " + e.getMessage());
        }

        return new Response(0, "Score des sets mis à jour");
    }
    
    
}
