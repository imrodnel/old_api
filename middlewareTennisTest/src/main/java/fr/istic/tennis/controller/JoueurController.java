package fr.istic.tennis.controller;

import fr.istic.tennis.model.sp.ListeJoueursProcedure;
import fr.istic.tennis.model.vo.ListeJoueursRequest;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RequestBody;

/**
 * Controleur chargé de la gestion des joueurs
 * @author Antoine
 */
@Controller
@RequestMapping(value = "/joueur")
public class JoueurController {
    
    @Autowired
    ListeJoueursProcedure procedureListeJoueurs;
    
    @RequestMapping(method = RequestMethod.POST, value = "/listeJoueurs")
    public @ResponseBody Object listeJoueurs(@RequestBody ListeJoueursRequest requete) {
        

        return procedureListeJoueurs.execute_query(requete);                 
    }  
}
