package fr.istic.tennis.controller;

import fr.istic.tennis.model.sp.InfoCourtProcedure;
import java.util.HashMap;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 *
 * @author Monik
 */

@Controller
@RequestMapping(value = "/court")
public class CourtController {
    
    @Autowired
    InfoCourtProcedure infoCourtProcedure;
    
    @RequestMapping(method = RequestMethod.GET, value = "/info/{numero}")
    public @ResponseBody HashMap<String,Object> getCourtInfo(@PathVariable int numero) {
        
        //System.out.println("----------- getCourtInfo ---------------");                
        return infoCourtProcedure.execute_query(numero);   
    } 
}
