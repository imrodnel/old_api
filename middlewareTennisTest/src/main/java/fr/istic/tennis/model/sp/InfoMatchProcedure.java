/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.istic.tennis.model.sp;

import java.sql.Types;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

/**
 *
 * @author Romain
 */
@Component
public class InfoMatchProcedure extends StoredProcedure implements ProcedureInterface {

     private static final String SQL = "InfoMatch";
    //​CALL InfosSuperMatchs (ident);

    @Autowired
    public InfoMatchProcedure(@Qualifier("dataSource") DataSource dataSource) {        
        super(dataSource, SQL);
        super.declareParameter(new SqlParameter("Idm", Types.INTEGER));
        setFunction(false);
        compile();
    }
    
    @Override
    public void execute_update(Object request) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object execute_query(Object request) {
        int ident=(int) request;
        Map<String, Object> in = new HashMap<>();
        in.put("Idm", ident);
        HashMap<String, Object> reponseBD = new HashMap<>();
        try{
            reponseBD  = (HashMap<String, Object>) super.execute(in);
        }catch(Exception e){
            reponseBD = new HashMap<>();
            reponseBD.put("code", "-1");
            reponseBD.put("message",e.getMessage());
            return reponseBD;
        }
         return buildResponse(reponseBD);
    }

    @Override
    public Object buildResponse(Map responseBD) {
       
        HashMap<Object, Object> res1 = ((ArrayList<HashMap<Object, Object>>) responseBD.get("#result-set-1")).get(0); //Resultset avec l'information des sets
        return res1;
    }
    
}
