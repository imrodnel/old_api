package fr.istic.tennis.model.sp;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

/**
 *
 * @author Monik
 */
@Component
public class InfoCourtProcedure extends StoredProcedure implements ProcedureInterface {

    private static final String SQL = "InfoCourt";
    //​CALL `InfoCourt` (Numéro Court);

    @Autowired
    public InfoCourtProcedure(@Qualifier("dataSource") DataSource dataSource) {
        super(dataSource, SQL);
        super.declareParameter(new SqlParameter("Court", Types.INTEGER));
        setFunction(false);
        compile();
    }

    @Override
    public HashMap<String, Object> execute_query(Object court) {
        Map<String, Object> in = new HashMap<>();
        in.put("Court", court);
        HashMap<String, Object> reponseBD = new HashMap<>();
        try {
            reponseBD = (HashMap<String, Object>) super.execute(in);
        } catch (Exception e) {
            System.out.println("Entro al catch  " + e);
            reponseBD = new HashMap<>();
            reponseBD.put("code", "-1");
            reponseBD.put("message", e.getMessage());
            return reponseBD;
        }
        //System.out.println("super.execute(in) " + reponseBD);
        return buildResponse(reponseBD);
    }

    /*  @Override
     public Object buildResponse(Map responseBD) {
     throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
     }*/
    @Override
    public HashMap<String, Object> buildResponse(Map reponseBD) {

        HashMap<String, Object> response = new HashMap<>();
        List<HashMap<String, Object>> sets = new ArrayList();

        ArrayList<HashMap<Object, Object>> res = (ArrayList<HashMap<Object, Object>>) reponseBD.get("#result-set-1"); //Resultset avec l'information des joueurs
        HashMap<String, Object> set = new HashMap();
        sets = new ArrayList();

        String id;


        for (HashMap r1 : res) {
            id = (String) r1.get("Ref");
            response.put("nomJoueur" + id, sortNames((String) r1.get("Nom")));
            response.put("nationalite" + id, r1.get("Nationalite"));

            for (int i = 1; i < 6; i++) {
                if (sets.size() < i) {
                    //System.out.println("Entra al if... "+sets.size());
                    if (r1.get("Set" + i) == null) {
                        break;
                    }
                    //System.out.println("Se va  a guardar en ["+id+"]>"+r1.get("Set"+i));

                    set = new HashMap();
                    set.put(id, r1.get("Set" + i));
                    //System.out.println("[SET**** "+i+ "]  "+set);

                    sets.add(set);
                    //System.out.println("[Sets "+i+ "]  "+sets);

                } else {
                    //System.out.println("Entra al else... "+set.size());
                    if (r1.get("Set" + i) == null) {
                        break;
                    }
                    //System.out.println("Se va  a guardar en ["+id+"]>"+r1.get("Set"+i));
                    //System.out.println("[Set "+i+ "-1]  "+sets.get(i-1));

                    sets.get(i - 1).put(id, r1.get("Set" + i));
                }
            }

            response.put("set", sets);
            response.put("score" + id, (r1.get("Score") != null ? new Integer((String) r1.get("Score")) : 0));

            response.put("tableau" + id, r1.get("Tableau"));
            response.put("nature" + id, r1.get("Categorie"));

            //Ajout du vainqueur et de la durée du match
            response.put("vainqueur", r1.get("Vainqueur"));
            response.put("duree", r1.get("Duree"));

            if ((Integer) r1.get("Service") == 1) {
                response.put("service", id);
            }

        }

        return response;
    }

    @Override
    public void execute_update(Object request) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    private String sortNames(String names) {
        //System.out.println("sortNames ["+ names+"]") ;
        String[] names_separes = (names.split("/"));

        int size_arr = names_separes.length;

        for (int i = 0; i < size_arr; i++) {
            names_separes[i] = names_separes[i].trim();
            //System.out.println("Después de limpiar ["+names_separes[i]+"]");
        }

        Arrays.sort(names_separes);


        String newString = "";
        for (int i = 0; i < size_arr; i++) {
            //System.out.println("names_separes[i] ["+ names_separes[i]+"]") ;
            if (i != 0) {
                newString = newString + " / " + names_separes[i];
            } else {
                newString = newString + names_separes[i];
            }

            //System.out.println("newString ["+ newString+"]") ;
        }

        //System.out.println("Antes de salir newString "+ newString) ;
        return newString;
    }
}
