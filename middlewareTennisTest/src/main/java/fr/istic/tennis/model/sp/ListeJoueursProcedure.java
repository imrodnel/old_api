/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.istic.tennis.model.sp;

import fr.istic.tennis.model.vo.ListeJoueursRequest;
import fr.istic.tennis.model.vo.Response;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

/**
 *
 * @author Antoine
 */
@Component
public class ListeJoueursProcedure extends StoredProcedure implements ProcedureInterface {
    
    private static final String SQL = "ListesJoueurs";
    //CALL ListeJoueurs(nation, sexe);
    
    @Autowired
    public ListeJoueursProcedure(@Qualifier("dataSource")  DataSource dataSource) {
        
       super(dataSource, SQL);        
       super.declareParameter(new SqlParameter("nation", Types.VARCHAR)); 
       super.declareParameter(new SqlParameter("sexe", Types.VARCHAR));
       setFunction(false);
       compile();
    }
    
    @Override
    public void execute_update (Object request) throws Exception{
    
        throw new UnsupportedOperationException("Not supported yet.");
    }   
    
    @Override
    public Object execute_query (Object request){
        
        ListeJoueursRequest req = (ListeJoueursRequest) request;
        Map<String, Object> in = new HashMap<>();
        
        //System.out.println("Appel à la procédure ListeJoueurs ['nation' ; " + req.getNation() + ", 'sexe' : " + req.getSexe());
        in.put("nation", req.getNation());
        in.put("sexe", req.getSexe());
        
        try{
            
            return buildResponse(execute(in));
        }
        catch(Exception e){
            
            return new Response(-1, "Erreur dans l'appel à la BDD : " + e.getCause());
        }
    }
    
    @Override
    public Object buildResponse(Map responseDB){
        
        return responseDB.get("#result-set-1");
    }
}
