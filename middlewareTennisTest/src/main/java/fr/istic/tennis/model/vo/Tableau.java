/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.istic.tennis.model.vo;

/**
 *
 * @author Romain
 */
public class Tableau {
    
    private String tableau;

    /**
     * @return the tableau
     */
    public String getTableau() {
        return tableau;
    }

    /**
     * @param tableau the tableau to set
     */
    public void setTableau(String tableau) {
        this.tableau = tableau;
    }
}
