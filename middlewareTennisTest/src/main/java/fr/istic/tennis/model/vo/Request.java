package fr.istic.tennis.model.vo;

import java.io.Serializable;

/**
 *
 * @author Antoine
 */
public class Request implements Serializable{
    
    private static final long serialVersionUID = -32543654754L;
    private int nombre;
    
    public Request(){}
    
    public int getNombre(){
        
        return nombre;
    }
    
    public void setNombre(int nombre){
        
           this.nombre = nombre;
    }
}
