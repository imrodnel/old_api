package fr.istic.tennis.model.sp;

import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

/**
 *
 * @author Monik
 */
@Component
public class IdMatchOldPointProcedure extends StoredProcedure implements ProcedureInterface {

    private static final String SQL = "IdMatchtoIdPoint";
    //​CALL `IdMatchtoIdPoint` (Numéro idMatch);

    @Autowired
    public IdMatchOldPointProcedure(@Qualifier("dataSource") DataSource dataSource) {
        super(dataSource, SQL);
        super.declareParameter(new SqlParameter("idMatch", Types.INTEGER));
        setFunction(false);
        compile();
    }

    @Override
    public Integer execute_query(Object request) {
        int idMatch = (Integer) request;
        Map<String, Object> in = new HashMap<>();
        in.put("idMatch", idMatch);
        HashMap<String, Object> reponseBD = new HashMap<>();
        try {
            reponseBD = (HashMap<String, Object>) super.execute(in);
        } catch (Exception e) {
            System.out.println("Error IdMatchOldPointProcedure.execute " + e.getMessage());
            return -100;
        }
        //System.out.println("super.execute(in) " + reponseBD);
        return reponseBD.get("max(id)") == null ? 0 : (Integer) reponseBD.get("max(id)");
    }

    @Override
    public void execute_update(Object request) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object buildResponse(Map responseBD) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
