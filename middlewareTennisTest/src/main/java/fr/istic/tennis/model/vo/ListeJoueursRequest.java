package fr.istic.tennis.model.vo;

/**
 * Une requête de la tablette renseignant les critères "sexe" et "nation" pour la génération d'une liste de joueurs
 * @author Antoine
 */
public class ListeJoueursRequest {
    
    private String nation;
    private String sexe;
    
    public String getNation(){
        
        return this.nation;
    }
    
    public String getSexe(){
        
        return this.sexe;
    }
    
    public void setNation(String nation){
        
        this.nation = nation;
    }
    
    public void setSexe(String sexe){
        
        this.sexe = sexe;
    }
}
