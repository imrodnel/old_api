package fr.istic.tennis.model.sp;

import java.util.Map;

/**
 *
 * @author Monik
 */
public interface ProcedureInterface {
    public void execute_update (Object request) throws Exception;    
    public Object execute_query (Object request);
    public Object buildResponse(Map responseBD);
}
