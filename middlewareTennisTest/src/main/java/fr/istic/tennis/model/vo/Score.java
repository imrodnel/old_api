package fr.istic.tennis.model.vo;

/**
 *
 * @author Monik
 */
public class Score {
    private int idPoint;
    private int idMatch;
    private int numeroSet;
    private int scoreA;
    private int scoreB;
    private int setA;
    private int setB;
    private int gameA;
    private int gameB;
    private int service;

    public int getIdPoint() {
        return idPoint;
    }

    public void setIdPoint(int idPoint) {
        this.idPoint = idPoint;
    }

    public int getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(int idMatch) {
        this.idMatch = idMatch;
    }

    public int getNumeroSet() {
        return numeroSet;
    }

    public void setNumeroSet(int numeroSet) {
        this.numeroSet = numeroSet;
    }

    public int getScoreA() {
        return scoreA;
    }

    public void setScoreA(int scoreA) {
        this.scoreA = scoreA;
    }

    public int getScoreB() {
        return scoreB;
    }

    public void setScoreB(int scoreB) {
        this.scoreB = scoreB;
    }

    public int getSetA() {
        return setA;
    }

    public void setSetA(int setA) {
        this.setA = setA;
    }

    public int getSetB() {
        return setB;
    }

    public void setSetB(int setB) {
        this.setB = setB;
    }

    public int getGameA() {
        return gameA;
    }

    public void setGameA(int gameA) {
        this.gameA = gameA;
    }

    public int getGameB() {
        return gameB;
    }

    public void setGameB(int gameB) {
        this.gameB = gameB;
    }

    public int getService() {
        return service;
    }

    public void setService(int service) {
        this.service = service;
    }

    @Override
    public String toString() {
        return "Score{" + "idPoint=" + idPoint + ", idMatch=" + idMatch + ", numeroSet=" + numeroSet + ", scoreA=" + scoreA + ", scoreB=" + scoreB + ", setA=" + setA + ", setB=" + setB + ", gameA=" + gameA + ", gameB=" + gameB + ", service=" + service + '}';
    }
    
    
   
}
