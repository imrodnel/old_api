package fr.istic.tennis.model;

import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.stereotype.Component;

/**
 *
 * @author Monik
 */

@Component
public class TestBaseModel {   
    private JdbcTemplate jdbcTemplate;
            
    @Autowired
    public void setDataSource(@Qualifier("dataSourceTest") DataSource dataSource) {
        jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
    public int insert(int nb){
        
        String sql = "CALL insertion("+nb+");";
        jdbcTemplate.execute(sql);

        return nb;
    }
    
    public int query(){
        
        String sql = "SELECT selectscore();";
        int max = jdbcTemplate.queryForInt(sql);
 
	return max;
    }
}
