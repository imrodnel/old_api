package fr.istic.tennis.model.sp;

import java.sql.Types;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

/**
 *
 * @author Monik
 */

@Component
public class InfosSuperMatchsProcedure extends StoredProcedure implements ProcedureInterface {    
    private static final String SQL = "InfosSuperMatchs";
    //​CALL InfosSuperMatchs ();

    @Autowired
    public InfosSuperMatchsProcedure(@Qualifier("dataSource") DataSource dataSource) {        
        super(dataSource, SQL);
        super.declareParameter(new SqlParameter("idEtap", Types.VARCHAR));
        setFunction(false);
        compile();
    }
        
    @Override
    public void execute_update(Object request) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object execute_query(Object request) {
        Map<String, Object> in = new HashMap<>();
        in.put("idEtap", request);
        HashMap<String, Object> responseBD  = (HashMap<String, Object>) super.execute(in);      
        HashMap<String, Object> response  = new HashMap();
        //System.out.println("Response InfosSuperMatchs MFA---- "+ responseBD);
        response.put("rencontre", buildResponse(responseBD));
        //System.out.println("Response execute_query MFA----"+ response);
        return response;
    }

    @Override
    public Object buildResponse(Map responseBD) {
        List<HashMap<String, Object>> resp_supermatch = new ArrayList(); //respuesta
        ArrayList<HashMap<String, Object>> bd=(ArrayList<HashMap<String, Object>>)responseBD.get("#result-set-1");        
        
        for(HashMap<String, Object> match_bd : bd){ //pour toutes les elements que on a recu de la BDD
            
            //Vrai si on itère sur un supermatch, sinon on traite un simple match
            if(!existSuperMatch((Integer)match_bd.get("IdSupmatch"),resp_supermatch)){
                //On ajoute le supermatch et l'information du match
                addSuperMatch(match_bd,resp_supermatch);
                addMatch(match_bd,resp_supermatch);
                
            }else{ //On ajoute seulement l'information du match pas le supermatch
                
                addMatch(match_bd,resp_supermatch);
            }            
        }
        
        //Calcul du score de chaque nation à partir du resultat
        calculScore(resp_supermatch);
        
        return resp_supermatch;
    }
    
    private boolean existSuperMatch(Integer idSuperMatch, List<HashMap<String, Object>> resp_supermatch){
        for(HashMap<String, Object> sm : resp_supermatch){            
            if((Integer)sm.get("IdSupmatch")==idSuperMatch){
                return true;
            }                            
        }
        return false;
    }
    
    private void addSuperMatch(HashMap<String, Object> supermatch, List<HashMap<String, Object>> resp_supermatch){
        HashMap<String, Object> supermatch_clean = new HashMap<String, Object>();
        supermatch_clean.put("IdSupmatch", supermatch.get("IdSupmatch"));
        supermatch_clean.put("Pays_A", supermatch.get("Pays_A"));
        supermatch_clean.put("Pays_B", supermatch.get("Pays_B"));
        supermatch_clean.put("Tableau", supermatch.get("Tableau"));

        resp_supermatch.add(supermatch_clean);
    }
    
    private void addMatch(HashMap<String, Object> supermatch, List<HashMap<String, Object>> resp_supermatch){
        
        HashMap<String, Object> new_match = new HashMap();
        List<HashMap<String, Object>> sets = new ArrayList();        
        
        for(HashMap<String, Object> superMatchResp : resp_supermatch){ //Trouver le supermatch pour ajouter le match            
            
            if(((Integer)superMatchResp.get("IdSupmatch"))==((Integer) supermatch.get("IdSupmatch"))){ //ajouter le match au superMatch
               
               new_match.put("Statut",supermatch.get("Statut"));
               new_match.put("Categorie",supermatch.get("Categorie"));
               new_match.put("IdMatch",supermatch.get("IdMatch"));
               new_match.put("Statut",supermatch.get("Statut"));//Ajout du statut pour la méthode calculScore()
               new_match.put("Vainqueur", supermatch.get("Vainqueur"));
                
               if (((String)supermatch.get("Statut")).equals("EN_COURS") || ((String) supermatch.get("Statut")).equals("TERMINE")){
                   new_match.put("Court",supermatch.get("Court"));
                   new_match.put("Score_A",supermatch.get("Score_A"));
                   new_match.put("Score_B",supermatch.get("Score_B"));
                   new_match.put("Joueurs_A",sortNames((String)supermatch.get("Joueurs_A")));
                   new_match.put("Joueurs_B",sortNames((String)supermatch.get("Joueurs_B")));
                   
                   //Ajouter les sets
                   sets = new ArrayList();        
                   for(int i=1; i<6; i++){
                       HashMap<String, Object> set = new HashMap();
                       set.put("A", supermatch.get("SetA"+i));
                       set.put("B", supermatch.get("SetB"+i));
                       
                       sets.add(set);
                   }
                   
                   new_match.put("set",sets);                   
                   
                   if (((String) supermatch.get("Statut")).equals("TERMINE")) {
                        new_match.put("set",sets);                   
                   }
               }
               if(superMatchResp.get("matchs")!=null){
                   ((List)superMatchResp.get("matchs")).add(new_match);
               }else{
                    superMatchResp.put("matchs", new ArrayList<HashMap<String, Object>>());
                    ((List)superMatchResp.get("matchs")).add(new_match);
               }                   
            }
        }
    }
    
    /**
     * Calcule le score de chaque nation d'un supermatch à partir de l'ensemble des matchs qu'elles ont disputé
     * @param supermatchs L'ensemble des supermatchs comprenant les matchs disputés entre les 2 nations concernées
     */
    private void calculScore(List<HashMap<String, Object>> supermatchs){
        
        //Pour chaque supermatch
        for(HashMap<String, Object> supermatch : supermatchs){
            
            int scoreA = 0, scoreB = 0;
            
            //Pour chaque match entre les 2 nations
            for(HashMap<String, Object> match : (List<HashMap<String, Object>>)supermatch.get("matchs")){

                //System.out.println(match);
                
                //On calcule le score à partir des matchs terminés
                if(((String)match.get("Statut")).equals("TERMINE")){
                 
                    String vainqueur = (String)match.get("Vainqueur");
                    
                    //Rajouté car la BDD n'est pas propre
                    if(vainqueur == null){
                        
                        continue;
                    }
                    
                    //On détermine le vainqueur
                    if(vainqueur.equals("A")){
                        
                        scoreA++;
                    }
                    else{
                        
                        scoreB++;
                    }
                } 
            }
            
            //Ajout du score au supermatch
            supermatch.put("ScorePays_A", String.valueOf(scoreA));
            supermatch.put("ScorePays_B", String.valueOf(scoreB));
        }
    }
    
     private String sortNames(String names){
        //System.out.println("sortNames ["+ names+"]") ;
        String[] names_separes = (names.split("/"));
        
        int size_arr= names_separes.length;
                
        for(int i=0; i<size_arr; i++){
           names_separes[i] = names_separes[i].trim();
           //System.out.println("Después de limpiar ["+names_separes[i]+"]");
        }

        Arrays.sort(names_separes);

        String newString= "";
        for(int i=0; i<size_arr; i++){
          //System.out.println("names_separes[i] ["+ names_separes[i]+"]") ;
          if(i!=0)
            newString =  newString+  " / " + names_separes[i] ;
          else 
            newString =  newString + names_separes[i];
          
            //System.out.println("newString ["+ newString+"]") ;
        }
        
            //System.out.println("Antes de salir newString "+ newString) ;
        return newString;
    }
}
