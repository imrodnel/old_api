/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.istic.tennis.model.vo;

/**
 *
 * @author Romain
 */
public class EndMatch {
    
    private String gagnant;
    private int idMatch;
    private String dateFin;

    /**
     * @return the gagnant
     */
    public String getGagnant() {
        return gagnant;
    }

    /**
     * @param gagnant the gagnant to set
     */
    public void setGagnant(String gagnant) {
        this.gagnant = gagnant;
    }

    /**
     * @return the idMatch
     */
    public int getIdMatch() {
        return idMatch;
    }

    /**
     * @param idMatch the idMatch to set
     */
    public void setIdMatch(int idMatch) {
        this.idMatch = idMatch;
    }

    public String getDateFin() {
        return dateFin;
    }

    public void setDateFin(String dateFin) {
        this.dateFin = dateFin;
    }
    
    
    
}
