package fr.istic.tennis.model.sp;

import fr.istic.tennis.model.vo.EndMatch;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

/**
 *
 * @author Romain Procedure stockée FinMatch
 */
@Component
public class FinMatchProcedure extends StoredProcedure implements ProcedureInterface {

    private static String SQL = "FinMatch";
    //CALL FinMatch( IdMatch, 'Gagnant', 'DateFin' );

    @Autowired
    public FinMatchProcedure(@Qualifier("dataSource") DataSource dataSource) {
        super(dataSource, SQL);
        super.declareParameter(new SqlParameter("IdMatch", Types.INTEGER));
        super.declareParameter(new SqlParameter("Gagnant", Types.VARCHAR));
        super.declareParameter(new SqlParameter("DateFin", Types.VARCHAR));

    }

    @Override
    public void execute_update(Object request) throws Exception {
        EndMatch endMatch = (EndMatch) request;
        if (!endMatch.getGagnant().equals("A") && !endMatch.getGagnant().equals("B")) {
            throw new Exception("Erreur nom du gagnant inconnu");
        }
        Map<String, Object> in = new HashMap<>();
        in.put("IdMatch", endMatch.getIdMatch());
        in.put("Gagnant", endMatch.getGagnant());
        in.put("DateFin", endMatch.getDateFin());
        super.execute(in);
    }

    @Override
    public Object execute_query(Object request) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object buildResponse(Map responseBD) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}