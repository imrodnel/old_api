package fr.istic.tennis.model.vo;

/**
 *
 * @author Monik
 */
public class Match {
    //Résultat : Nom (Nom), nationalité (Nationalité) des joueurs, les set(Set1...), 
    //le score(Score), quel joueur en service (Service)
    //_**Service**_ (type boolean): O,1 Le O représente l'equipe A et le 1 l'equipe B
    
    private String nom;
    private String nationalite;
    private int set1;
    private int set2;
    private int set3;
    private int set4;
    private int set5;
    private int score;
    private boolean service;

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getNationalite() {
        return nationalite;
    }

    public void setNationalite(String nationalite) {
        this.nationalite = nationalite;
    }

    public int getSet1() {
        return set1;
    }

    public void setSet1(int set1) {
        this.set1 = set1;
    }

    public int getSet2() {
        return set2;
    }

    public void setSet2(int set2) {
        this.set2 = set2;
    }

    public int getSet3() {
        return set3;
    }

    public void setSet3(int set3) {
        this.set3 = set3;
    }

    public int getSet4() {
        return set4;
    }

    public void setSet4(int set4) {
        this.set4 = set4;
    }

    public int getSet5() {
        return set5;
    }

    public void setSet5(int set5) {
        this.set5 = set5;
    }

    public int getScore() {
        return score;
    }

    public void setScore(int score) {
        this.score = score;
    }

    public boolean isService() {
        return service;
    }

    public void setService(boolean service) {
        this.service = service;
    }
    
}
