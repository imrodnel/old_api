package fr.istic.tennis.model.vo;

/**
 *
 * @author Monik
 */
public class ResponsePoint extends Response{
    private int idOldPoint;

    public ResponsePoint() {
    }

    public ResponsePoint(int idOldPoint, int code, String message) {
        super(code, message);
        this.idOldPoint = idOldPoint;
    }
    
    public int getIdOldPoint() {
        return idOldPoint;
    }

    public void setIdOldPoint(int idOldPoint) {
        this.idOldPoint = idOldPoint;
    }  
}
