package fr.istic.tennis.model.sp;

import fr.istic.tennis.model.vo.Score;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

/**
 *  
 * @author Monik
 */

@Component
public class UpdateTotProcedure extends StoredProcedure implements ProcedureInterface {
           
    private static String SQL = "UpdateTot";
    //CALL UpdateTot (int,int,int,int,int,int,int,int,int,boolean)   
    
    @Autowired
    public UpdateTotProcedure(@Qualifier("dataSource") DataSource dataSource) {
        super(dataSource, SQL);
        
        super.declareParameter(new SqlParameter("IdPoint", Types.INTEGER));
        super.declareParameter(new SqlParameter("IdMatch", Types.INTEGER));
        super.declareParameter(new SqlParameter("NumeroSet", Types.INTEGER));
        super.declareParameter(new SqlParameter("SetA", Types.INTEGER));
        super.declareParameter(new SqlParameter("SetB", Types.INTEGER));        
        super.declareParameter(new SqlParameter("GameA", Types.INTEGER));
        super.declareParameter(new SqlParameter("GameB", Types.INTEGER));
        super.declareParameter(new SqlParameter("ScoreA", Types.INTEGER));
        super.declareParameter(new SqlParameter("ScoreB", Types.INTEGER));
        super.declareParameter(new SqlParameter("Service", Types.BOOLEAN));

        setFunction(false);
        compile();    
    }  
    
   @Override 
   public void execute_update(Object request) {
        Score score = (Score) request;
        Map<String, Object> in = new HashMap<>();
        in.put("IdPoint", score.getIdPoint());
        in.put("IdMatch", score.getIdMatch());
        in.put("NumeroSet", score.getNumeroSet());
        in.put("SetA", score.getSetA());
        in.put("SetB", score.getSetB());
        in.put("GameA", score.getGameA());
        in.put("GameB", score.getGameB());
        in.put("ScoreA", score.getScoreA());
        in.put("ScoreB", score.getScoreB());
        in.put("Service", score.getService());

        super.execute(in);      
    }    
   
    @Override
    public Object execute_query(Object request) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object buildResponse(Map responseBD) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
