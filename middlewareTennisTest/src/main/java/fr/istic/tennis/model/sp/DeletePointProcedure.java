/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.istic.tennis.model.sp;

import fr.istic.tennis.model.vo.Score;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

/**
 *
 * @author Romain
 */
@Component
public class DeletePointProcedure extends StoredProcedure implements ProcedureInterface {
    
     private static String SQL = "SupprPoint";
    //CALL DeletePoint( IdMatch, IdPoint );
    
    @Autowired
    public DeletePointProcedure(@Qualifier("dataSource")  DataSource dataSource) {
        super(dataSource, SQL);        
        super.declareParameter(new SqlParameter("IdMatch", Types.INTEGER)); 
        super.declareParameter(new SqlParameter("IdPoint", Types.INTEGER)); 
    }
    
    

    @Override
    public void execute_update(Object request) throws Exception {
        Score score=(Score) request;
        Map<String, Object> in = new HashMap<>();
        in.put("IdMatch", score.getIdMatch());
        in.put("IdPoint", score.getIdPoint());
        
        super.execute(in); 
    }

    @Override
    public Object execute_query(Object request) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object buildResponse(Map responseBD) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
