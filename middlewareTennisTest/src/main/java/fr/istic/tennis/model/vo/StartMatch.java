package fr.istic.tennis.model.vo;

/**
 * 
 * @author Monik
 */
public class StartMatch {
    private int idMatch;
    private int idCourt;
    private String startDate;
    private String service;
    private int idJoueurA_Eq1;
    private int idJoueurB_Eq1;
    private int idJoueurA_Eq2;
    private int idJoueurB_Eq2;
    
    public int getIdMatch() {
        return idMatch;
    }

    public void setIdMatch(int idMatch) {
        this.idMatch = idMatch;
    }

    public int getIdCourt() {
        return idCourt;
    }

    public void setIdCourt(int idCourt) {
        this.idCourt = idCourt;
    }

    public String getStartDate() {
        return startDate;
    }

    public void setStartDate(String startDate) {
        this.startDate = startDate;
    }

    public String getService() {
        return service;
    }

    public void setService(String service) {
        this.service = service;
    }

    public int getIdJoueurA_Eq1() {
        return idJoueurA_Eq1;
    }

    public void setIdJoueurA_Eq1(int idJoueurA_Eq1) {
        this.idJoueurA_Eq1 = idJoueurA_Eq1;
    }

    public int getIdJoueurB_Eq1() {
        return idJoueurB_Eq1;
    }

    public void setIdJoueurB_Eq1(int idJoueurB_Eq1) {
        this.idJoueurB_Eq1 = idJoueurB_Eq1;
    }

    public int getIdJoueurA_Eq2() {
        return idJoueurA_Eq2;
    }

    public void setIdJoueurA_Eq2(int idJoueurA_Eq2) {
        this.idJoueurA_Eq2 = idJoueurA_Eq2;
    }

    public int getIdJoueurB_Eq2() {
        return idJoueurB_Eq2;
    }

    public void setIdJoueurB_Eq2(int IdJoueurB_Eq2) {
        this.idJoueurB_Eq2 = IdJoueurB_Eq2;
    }
    
    
}
