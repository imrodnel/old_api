package fr.istic.tennis.model.vo;

import java.io.Serializable;

/**
 *
 * @author Monik
 */
public class Response implements Serializable{
    
    private static final long serialVersionUID = -325436547754L;
    private int code=0;
    private String message="Exito (Instance non modifiée)";

    public Response() {
    }

    public Response(String  message) {
        this.message=message;
    }
    
    public Response(int code, String  message) {
        this.code=code;
        this.message=message;
    }
    
    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
