package fr.istic.tennis.model.sp;

import fr.istic.tennis.model.vo.UpdateSet;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

/**
 *
 * @author Antoine
 */

@Component
public class UpdateSetProcedure extends StoredProcedure implements ProcedureInterface {
    
    //Appel à la procédure Update1SetAffi
    private static final String SQL = "Update1SetAffi";  
    
    @Autowired
    public UpdateSetProcedure(DataSource dataSource) {        
        super(dataSource, SQL);        
        super.declareParameter(new SqlParameter("IdMatch", Types.INTEGER));
        super.declareParameter(new SqlParameter("NumSet", Types.INTEGER));
        super.declareParameter(new SqlParameter("SetA", Types.INTEGER));
        super.declareParameter(new SqlParameter("SetB", Types.INTEGER));

        setFunction(false);
        compile();    
    }  
    
   @Override
   public void execute_update(Object request) {
        UpdateSet infos = (UpdateSet) request;
       Map<String, Object> in = new HashMap<>();
        
        in.put("IdMatch", infos.getIdMatch());
        in.put("NumSet", infos.getNumSet());
        in.put("SetA", infos.getSetA());
        in.put("SetB", infos.getSetB());

        super.execute(in);      
    }    

    
    @Override
    public Object execute_query(Object request) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object buildResponse(Map responseBD) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
