package fr.istic.tennis.model.vo;

/**
 *
 * @author Antoine
 */
public class UpdateSet {
    
    private int idMatch;
    private int numSet;
    private int setA;
    private int setB;
    
    public int getIdMatch(){
        
        return idMatch;
    }
    
    public int getNumSet(){
        
        return numSet;
    }
    
    public int getSetA(){
        
        return setA;
    }
    
    public int getSetB(){
        
        return setB;
    }
    
    public void setIdMatch(int idMatch){
        
        this.idMatch = idMatch;
    }
    
    public void setNumSet(int numSet){
        
        this.numSet = numSet;
    }
    
    public void setSetA(int setA){
        
        this.setA = setA;
    }
    
    public void setSetB(int setB){
        
        this.setB = setB;
    }
}
