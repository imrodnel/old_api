package fr.istic.tennis.model.sp;

import fr.istic.tennis.model.vo.Response;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

/**
 *
 * @author Antoine
 */
@Component
public class InfosTableauProcedure extends StoredProcedure implements ProcedureInterface {

    private static final String SQL = "InfoTableau";
    //CALL InfoTableu();
    
    @Autowired
    public InfosTableauProcedure(@Qualifier("dataSource")  DataSource dataSource) {
        
       super(dataSource, SQL);        
       setFunction(false);
       compile();
    }
    
    @Override
    public void execute_update(Object request) throws Exception {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object execute_query(Object request) {
        
        try{
            
            return buildResponse(execute());
        }
        catch(Exception e){
            
            return new Response(-1, "Erreur dans l'appel à la BDD : " + e.getCause());
        }
    }

    @Override
    public Object buildResponse(Map responseDB) {
        
        return responseDB.get("#result-set-1");
    }    
}
