package fr.istic.tennis.model.sp;

import fr.istic.tennis.model.vo.StartMatch;
import java.sql.Types;
import java.util.HashMap;
import java.util.Map;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.jdbc.core.SqlParameter;
import org.springframework.jdbc.object.StoredProcedure;
import org.springframework.stereotype.Component;

/**
 *
 * @author Monik
 */

@Component
public class DebutMatchProcedure extends StoredProcedure implements ProcedureInterface {
	
     private static String SQL = "DebutMatch";
    //call DebutMatch( IdMatch : int, NumeroCourt : int ,DateDebut : de la forme 2014-11-12 23:59:00
    //Service : boolean, IdJoueurA_Eq1 : int, IdJoueurB_Eq1 : int, IdJoueurA_Eq2 : int, IdJoueurB_Eq2 : int)
    
    @Autowired
    public DebutMatchProcedure(@Qualifier("dataSource")  DataSource dataSource) {
        super(dataSource, SQL);        
        super.declareParameter(new SqlParameter("idm", Types.INTEGER)); 
        super.declareParameter(new SqlParameter("numcourt", Types.INTEGER)); 
        super.declareParameter(new SqlParameter("datedebut", Types.TIMESTAMP));
        super.declareParameter(new SqlParameter("service", Types.CHAR)); 
        super.declareParameter(new SqlParameter("idJoueurA_Eq1", Types.INTEGER)); 
        super.declareParameter(new SqlParameter("idJoueurB_Eq1", Types.INTEGER)); 
        super.declareParameter(new SqlParameter("idJoueurA_Eq2", Types.INTEGER)); 
        super.declareParameter(new SqlParameter("idJoueurB_Eq2", Types.INTEGER)); 

    }
    
    @Override
    public void execute_update(Object request) throws Exception {         
        StartMatch startMatch = (StartMatch) request;
        Map<String, Object> in = new HashMap<>();
        in.put("idm", startMatch.getIdMatch());
        in.put("numcourt", startMatch.getIdCourt());
        in.put("datedebut", startMatch.getStartDate());
        in.put("service", startMatch.getService().charAt(0));
        in.put(("idJoueurA_Eq1"), startMatch.getIdJoueurA_Eq1());
        in.put("idJoueurB_Eq1",startMatch.getIdJoueurB_Eq1());
        in.put("idJoueurA_Eq2",startMatch.getIdJoueurA_Eq2());
        in.put("idJoueurB_Eq2",startMatch.getIdJoueurB_Eq2());

        HashMap<String, Object> reponseBD  = (HashMap<String, Object>) super.execute(in);      
        
        //System.out.println("reponseBD "+reponseBD);
    }    

    @Override
    public Object execute_query(Object request) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public Object buildResponse(Map responseBD) {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
}
